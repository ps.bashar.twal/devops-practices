commitHash=$(git log -n 1 --format='%H' | cut -c 1-8)
commitDate=$(git log -n 1 --format='%cd' --date=short | date '+%y%m%d')
echo "$commitDate-$commitHash"
