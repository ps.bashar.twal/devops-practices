FROM java:8-jdk-alpine
ENV db_profile=" "
COPY ./target/*.jar /u01/app/practiceApp.jar
WORKDIR /u01/app
EXPOSE 8090
ENTRYPOINT java -jar $db_profile /u01/app/practiceApp.jar
